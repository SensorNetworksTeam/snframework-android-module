# SNFramework

The Sensor Networks Android framework interacting with the Sensor Networks Platform.

## Getting Started

These instructions will get this framework setup into your Androiud Studio project. At the moment it is distributed via the Git repo only but in the future we will provide a gradle compile.


### Prerequisites

This project is a native Android application, so to build it you would need a machine setup with Android Studio.

### Installing

To use the SNFramework in the Android Studio project, you will need to add it as a AAR module. To do this open `File - New - New Module` which will open the New Module screen. Choose the `Import .JAR / .AAR ` button and click `next` . Continue with the wizard and find the snframework-release.aar file to import the module. Give it a subproject name and click `finish`. The SNFramework modue will now be imported into you project.

The SNFramework module needs a few third other third party libraries to be added. This is done easily by adding the following to your application gradle file:

```
compile 'io.swagger:swagger-annotations:1.5.8'
compile 'com.squareup.okhttp:okhttp:2.7.5'
compile 'com.squareup.okhttp:logging-interceptor:2.7.5'
compile 'joda-time:joda-time:2.9.3'
compile 'com.google.code.gson:gson:2.6.2'
compile 'com.neovisionaries:nv-websocket-client:1.31'
```

Once you have added the following libraries to your gradle file, it will sync after which the SNFramework module will be ready to be used in your application.

## Note

The installation right now is manual but we plan to have this available as a gradle install via maven or jcentre. 

### Using the framework
The main class is a singleton called `SNFramework` . In order to initialize it, you will have to pass a class observing the SNFrameworkCallback implementation. The second paramter is a Context needed by the SNFramework.

```
snframework = SNFramework.getInstance(self,self); 
```

This is the SNFramework connection and login callback methods. the following callbacks will be called when the framework connects, logins , logout & disconnects

```
	@Override
    public void connected() {

    }

    @Override
    public void disConnected() {

    }

	@Override
    public void connectError() {
    
    }

    @Override
    public void didLogin() {

    }

    @Override
    public void didSuccedRequest(String s, Object o) {

    }

    @Override
    public void didFailRequest(String s, String s1) {

    }
```
Usually this is handled by a class extending the Application class, so that the app can handle the connection and disconnection graciously without having to worry about what Activity is the foreground.

To initialise the framework, the following needs to be done:

```
	public void connectSNFramework(){
        final AppContext self = this;

        //websocketmanager
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    snframework = SNFramework.getInstance(self,self);
                    try {

                        snframework.verbose = true;
                        snframework.setDev();
                        snframework.connect();
                        Log.d(TAG, "Connecting...");
                    } catch (IOException e) {
                        // handle graciously
                    } catch (WebSocketException e) {
                        //handle graciously
                    }
                } catch (Exception e) {
                    // handle grciously
                }
            }
        });

        thread.start();
 	}
```

Once the framework has connected to the Sensor Networks Platform, it will callback into the `connected()` method.

## Authentication

The platform utilizes OAuth to authenticate, so we have limited the authentication of the server to third party providers. Once you are setup as a provider for the Sensor Networks Platform, you will be able to login into the SNFramework by passing a Accesstoken. This Accesstoken will be provided via your own Authentication system and used by our Sensor Networks Platform to validate against your Authentication system. To login once the framework has connected, you do the following, usually in the didConnect callback method: 

```
	@Override
    public void connected() {
    	snframework.login(accessToken);
    }
```
The framework will authenticate and callback into the didLogin method.

## Fetching device data 
Once you have logged in you can fetch a list of devices that is available to you via the Sensor Networks Platform. At the moment we have 2 IoT devices available, the SNGeyser & the SNTracker. This list will exand in the future as our list of devices grows. Fetching the list of geysers is as easy as the following:

```
	public void didLogin() {
        try {
            snframework.fetchGeysers();
        } catch (SNFrameworkException e) {
            switch (e.error_type){
                case NOT_CONNECTED:
                    showNetworkFailAlert();
            }
        }
    }
    
```

## Recieving device data
The SNFramework uses websockets as a backend and manages the requests and responses internally. As these can happen asynchronously the application will have to register BroadcastRecievers in order to listen for responses from the framework. Each activity will only have to listen for the broadcast that it needs. 

The following example is from an Activity that displays a list of SNGeysers:

```
	//register snframework broadcast reciever
    registerReceiver(snframeworkBroadcastReceiver, Helper.makeSNFrameworkGeyserFilter());
```

This is the intentFilter:

```
	public static IntentFilter makeSNFrameworkGeyserFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(SNFramework.NOTIFICATION_GEYSER_UPDATED);
        intentFilter.addAction(SNFramework.NOTIFICATION_GEYSER_LOADED);
        intentFilter.addAction(SNFramework.NOTIFICATION_GEYSERS_COLLECTION_LOADED);

        return intentFilter;
    }
```

This is the broadcastreciever:

```
	private final BroadcastReceiver snframeworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(SNFramework.NOTIFICATION_GEYSERS_COLLECTION_LOADED.equals(intent.getAction())) {
                // display geyser list
            }else if(SNFramework.NOTIFICATION_GEYSER_UPDATED.equals(intent.getAction())){
                //refresh geyser list
            }
        }
    };

```


## Built With
* [nv-websocket-client](https://github.com/TakahikoKawasaki/nv-websocket-client) - Websockets library.
* [https://swagger.io/](https://swagger.io/) - The worlds most popular framework for API's.
* [GSON](https://github.com/google/gson) - A Java serialization/deserialization library to convert Java Objects into JSON and back.
* [OkHttp](http://square.github.io/okhttp/) - An HTTP & HTTP/2 client for Android and Java applications.


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/SensorNetworksTeam/snframework-android-module). 

## Authors

* **Bradley Wells** * bradley@sensornetworks.com

## License

This project is licensed under the Propriety license on behalf of Sensor Networks - see the [LICENSE.md](LICENSE.md) file for details